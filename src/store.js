import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex);

const moduleGame = {
    state: {
        coins: 0,
        goCount: 0,
        leftCount: 0,
        helicopterTop: 1,
        nextActive: false,
        engellerLength: 10,
        engeller: [],
    },
    mutations: {}
};

const moduleDashboard = {
    state: {
        totalCoins: 0,
        nextLevel: 0,
        upLevel: 0,
        gaming: false,
        settings: false,
        shop: true,
        autoNext: false,
        nightMode: true,
        helicopterColors: [
            {sold: true, checked: true, color: 'black'},
            {sold: false, checked: false, color: 'orange'},
            {sold: false, checked: false, color: 'red'},
            {sold: false, checked: false, color: 'green'},
        ],
        selectedHelicopterColor: null,
        coinsArray: [],
    },
    mutations: {
        openGaming(state) {
            state.gaming = true;
            state.settings = false;
            state.shop = false;
        },

        openSettings(state) {
            state.gaming = false;
            state.settings = true;
            state.shop = false;
        },

        openShop(state) {
            state.gaming = false;
            state.settings = false;
            state.shop = true;
        },

        nextLevel(state) {
            if (state.totalCoins >= 100) {
                state.nextLevel += 1;
                state.totalCoins -= 100;
            }
        },

        upLevel(state) {
            if (state.totalCoins >= 100) {
                state.upLevel += 1;
                state.totalCoins -= 100;
            }
        },

        helicopterColor(state, helicopterColor) {
            if (helicopterColor.sold && !helicopterColor.checked) {
                state.helicopterColors.forEach(function (color) {
                    if (color.color !== helicopterColor.color) {
                        color.checked = false;
                    }
                    helicopterColor.checked = true;
                    state.selectedHelicopterColor = helicopterColor.color;
                });
            }
            else if (!helicopterColor.sold && state.totalCoins >= 100) {
                helicopterColor.sold = true;
                this.$store.state.dashboard.totalCoins -= 100;
            }
        },

        totalCoinsCalculation(state, dashboardCoin) {
            state.totalCoins += dashboardCoin;
        },

        engelLengthMethod() {
            this.state.game.engeller = [];
            var i = 0;
            do {
                i++;
                const engel = {
                    id: i,
                    topHeight: Math.floor(Math.random() * (40 - 30 + 1)) + 30,
                    bottomHeight: Math.floor(Math.random() * (45 - 30 + 1)) + 30,
                };
                this.state.game.engeller.push(engel)
            } while (i < this.state.game.engellerLength);
        },
    }
};

const vuexLocal = new createPersistedState({
    storage: window.localStorage
});

const store = new Vuex.Store({
    modules: {
        game: moduleGame,
        dashboard: moduleDashboard,
    },
    plugins: [vuexLocal]
});

export default store
